package main

import (
        "fmt"
        "math/big"
        "math"
        "strconv"
       )

func main(){
  i1, i2, i3 := 12, 1, 2
  sum := i1+i2+i3
  fmt.Println( "sum= ", sum)
  
  f1, f2, f3 := 1.1, 63.2, 1.1
  fsum := f1+f2+f3
  fmt.Println( "sum= ", fsum)
  
  var b1, b2, b3, bigSum big.Float
  b1.SetFloat64(1.1)
  b2.SetFloat64(63.2)
  b3.SetFloat64(1.1)
  bigSum.Add(&b1, &b2).Add( &b2, &b3)
  fmt.Printf( "big sum=%.10g\n ", &bigSum)
  
  //circleRadius := 15.5
  var circleRadius string
  fmt.Scan( &circleRadius )
  circumference = (strconv.ParseFloat(circleRadius, 32)) * math.Pi
  fmt.Printf("Circumference: %.2f\n", circumference)

}