// Go's concurrency primitives make it easy to
// express concurrent concepts, such as
// this binary tree comparison.
//
// Trees may be of different shapes,
// but have the same contents. For example:
//
//        4               6
//      2   6          4     7
//     1 3 5 7       2   5
//                  1 3
//
// This program compares a pair of trees by
// walking each in its own goroutine,
// sending their contents through a channel
// to a third goroutine that compares them.

package main

import (
	"fmt"
	"math/rand"
)

// A Tree is a binary tree with integer values.
type Tree struct {
	Left  *Tree
	Value int
	Right *Tree
}

// Walk traverses a tree depth-first,
// sending each Value on a channel.
func Walk(t *Tree, ch chan int) {
	if t == nil {
		return
	}
	//send the left part of the tree to be iterated over first
  Walk(t.Left, ch)
	ch <- t.Value
	Walk(t.Right, ch)
}

// Walker launches Walk in a new goroutine,
// and returns a read-only channel of values.
func Walker(t *Tree) <-chan int {
	ch := make(chan int)
	go func() {
		Walk(t, ch)
		close(ch)
	}()
	return ch
}

// Compare reads values from two Walkers
// that run simultaneously, and returns true
// if t1 and t2 have the same contents.
func Compare(t1, t2 *Tree) bool {
	c1, c2 := Walker(t1), Walker(t2)
	for {
		v1, ok1 := <-c1
		v2, ok2 := <-c2
		if !ok1 || !ok2 {
			return ok1 == ok2
		}
		if v1 != v2 {
			break
		}
	}
	return false
}

// New returns a new, random binary tree
// holding the values 1k, 2k, ..., nk.
func New(n, k int) *Tree {
	var t *Tree
	for _, v := range rand.Perm(n) {
		t = insert(t, (1+v)*k)
    //t.Size++
	}
	return t
}

func insert(t *Tree, v int) *Tree {
	if t == nil {
		return &Tree{nil, v, nil}
	}
	if v < t.Value {
		t.Left = insert(t.Left, v)
		return t
	}
	t.Right = insert(t.Right, v)
	return t
}

//func (root *Tree, v int) insert(new_node *Tree) {
//	if (new_node.value > root.value) {
//		if (root.right == nil) {
//			root.right = new_node
//		} else {
//			root.right.insert(new_node)
//		}
//	} else if (new_node.value < root.value) {
//		if (root.left == nil) {
//			root.left = new_node
//		} else {
//			root.left.insert(new_node)
//		}
//	}
//}

//func (tree *Bst) Insert(value int) {
//	if tree.root == nil {
//		tree.root = &Node{nil, nil, value}
//	}
//	tree.size++
//	tree.root.insert(&Node{nil, nil, value})
//}

type Bst struct{
  root *Tree;
  size int;
}

/* Get size of the tree */
func (bst *Bst) Size() int {
	return bst.size
}

/* Get size of the tree */
func (bst *Bst) Root() *Tree {
  return bst.root
}


// Construtor
func NewBst( t *Tree ) *Bst {
	bst := new(Bst)
	bst.root = t
  //bst.size = len(Walker(t))
  
  ch:= Walker(t)
  s :=0
  for range(ch){
    s++
  }
  bst.size  = s
	return bst
}

//func NewBst(n, k int) *Tree {
//	var t *Tree
//	for _, v := range rand.Perm(n) {
//		t = insert(t, (1+v)*k)
//    //t.Size++
//	}
//	return t
//}


// Print the tree in-order
// Traverse the left sub-tree, root, right sub-tree
func showInOrder(root *Tree) {
	if (root != nil) {
		showInOrder(root.Left)
		fmt.Print(root.Value)
    fmt.Print(" ")
		showInOrder(root.Right)
	}
}

// Print the tree pre-order
// Traverse the root, left sub-tree, right sub-tree
func showPreOrder(root *Tree) {
	if (root != nil) {
		fmt.Print(root.Value)
    fmt.Print(" ")
		showInOrder(root.Left)
		showInOrder(root.Right)
	}
}

// Print the tree post-order
// Traverse left sub-tree, right sub-tree, root
func showPostOrder(root *Tree) {
	if (root != nil) {
		fmt.Print(root.Value)
    fmt.Print(" ")
		showInOrder(root.Left)
		showInOrder(root.Right)
	}
}

func main() {
  t1 := New(11, 1)
  t2 := New(11, 1)
	fmt.Println(Compare(t1, t2), "Same Contents")
	fmt.Println(Compare(t1, New(9, 1)), "Differing Sizes")
	fmt.Println(Compare(t1, New(10, 2)), "Differing Values")
	fmt.Println(Compare(t1, New(11, 2)), "Dissimilar")

  //showInOrder(t1)
  fmt.Println("\nt1")
  showPreOrder(t1)
  fmt.Println("\nt2")
  showPreOrder(t2)

  // Number of nodes in the tree
	T := NewBst( t1 )
	T2 := NewBst( t2 )
  fmt.Println("\nT1 Size = ",T.Size())
  fmt.Println("T2 Size = ",T2.Size())
  
}