package main

import (
  "fmt"
  "sort"
)

func main(){
  //m := make(map[string]int) // wrap to prevent panic with make; use make vs new
  //m["key"] = 42
  //fmt.Println(m)
  
  states := make(map[string]string)
  fmt.Println(states)
  
  states["ny"] = "new york"
  states["fl"] = "florida"
  states["ca"] = "california"
  fmt.Println(states)
  
  florida := states["fl"]
  fmt.Println(florida)
  
  delete(states, "fl")
  fmt.Println(states)
  
  states["fl"] = "florida"
  
  for k,v := range states{ //loop , sort is not ordered!!
    fmt.Printf("%v: %v \n", k,v)
  }
  
  keys := make([]string, len(states))
  
  i:=0
  for k:= range states{ // use only k for key
    keys[i] = k
    i++
  }
  
  sort.Strings(keys)
  fmt.Println("\nSorted")
  
  for i := range keys{
    fmt.Println(states[keys[i]])
  }
  
}
