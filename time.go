package main

import (
  "fmt"
  "time"
)

func main(){
    t := time.Date(2009, time.November, 10,23,0,0,0, time.UTC)
    fmt.Printf("Time %s\n", t )
    
    now := time.Now()
    fmt.Printf("time now %s\n",now)
    fmt.Printf("week %s\n", now.Weekday())
    
    tomorrow := now.AddDate(0,0,1)
    fmt.Printf("tmrw %v %v %v %v",tomorrow.Weekday(), tomorrow.Month(), tomorrow.Day(), tomorrow.Year())
    shortFormat := "2/1/06"
    fmt.Println("tmrw short format", tomorrow.Format(shortFormat))
}