package main

import(
  "fmt"
)

func main(){
  defer fmt.Println("close the file") // defer LIFO
  fmt.Println("open the file")
  
  defer fmt.Println("Statement 1")
  defer fmt.Println("Statement 2")
  myFunc()
  defer fmt.Println("Statement 3")
  defer fmt.Println("Statement 4")
  defer fmt.Println("Statement 5")
  
}

func myFunc(){
  defer fmt.Println("deffered in the function")
  fmt.Println("not deffered in the function")
}
