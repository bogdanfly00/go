package main

import (
        "fmt"
        "stringutil"
       )

func main(){
  n1, l1 := stringutil.FullName("John", "Alice")
  fmt.Printf("Fullname %v number of chars %v",n1, l1 )
  n2, l2 := stringutil.FullNameNakedReturn("Doe", "McNeil")
  fmt.Printf("Fullname %v number of chars %v",n2, l2 )

}

