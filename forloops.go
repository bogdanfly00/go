package main

import (
  "fmt"
)

func main(){
  sum := 1
  fmt.Println("Sum:", sum)
  
  colors := []string{"Red","Green","Blue"}
  fmt.Println(colors)
  
  sum = 0
  
  for i := 0; i < 10; i++ {
    sum += i
  }
  fmt.Println(sum)
  
  for i :=0;i < len(colors ); i++ {
    fmt.Println(colors[i])
  }
  
  for i := range colors { //the same as the log loop
    fmt.Println(colors[i])
  }
  
  sum = 1
  for sum < 1000 { //loop
    sum += sum
    fmt.Println(sum)
    
    if sum > 200 { // break to label !!
      goto endofprogram
    }
    if sum > 500 { //break example
      break;
    }
  }
  endofprogram : fmt.Println("end of ptoram")  //label
  
}