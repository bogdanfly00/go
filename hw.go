package main

import (
	"fmt"
	"strings"
  "bufio"
  "os"
  "strconv"
)

func main() {
	fmt.Println("Hello from go")
	fmt.Println(strings.ToUpper("hello really lowd!"))
  //var s string
  //fmt.Scanln( &s )
  fmt.Println("enter text: ")
  reader := bufio.NewReader(os.Stdin)
  str, _ := reader.ReadString('\n')
  fmt.Println("my string ",str)

  fmt.Println("enter a number: ")
  str, _ = reader.ReadString('\n')
  f, err := strconv.ParseFloat( strings.TrimSpace(str), 64 )
  if err != nil {
    fmt.Println(err)
  }else{
    fmt.Println("my string ",f)
  }
    
}