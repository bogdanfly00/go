package main

import ("fmt")

func main(){

    a := []int{7, 2, 8, -9, 4, 0, 1, -1}
    cc := make(chan int)
    go sum (a[:len(a)/2], cc)
    go sum (a[len(a)/2:], cc)
    x, y := <-cc, <-cc //recieve from c
    
    fmt.Println("x, y sum=",x, y, x+y)
}

func sum( a []int, c chan int ){
  sum := 0
  for _, v := range a {
    sum += v
  }
  c <- sum //send sum to c
}