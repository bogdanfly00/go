package main

import (
  "fmt"
  "os"
  "errors"
)

func main(){
  f,err := os.Open("filename.ext")
  
  if err == nil {
    fmt.Println(f)
  } else {
    fmt.Println(err.Error())
  }
  
  myError := errors.New("My error string") //creating a new error
  fmt.Println( myError )
  
  attendance := map[string]bool{
    "Ann" : true,
    "Mike": true }
  attended, ok := attendance["Mike"] // ok a boolean value in go, swithc Mike with m
  
  if ok {
    fmt.Println("Mike attended ?", attended)
  } else {
    fmt.Println("no info for mike")
  }
  
    
}