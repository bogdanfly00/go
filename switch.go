package main

import(
  "fmt"
  "math/rand"
  "time"
)

func main(){
  rand.Seed(time.Now().Unix())
  dow := rand.Intn(6) + 1   //return a val 0-6 +1
  fmt.Println("day" ,dow)
  
  result := ""
  
  switch dow {
    case 1:
      result = "it's sunday"
    case 7:
      result = "itr's saturday"
    default:
      result = "weekday"
  }
  fmt.Println("day", dow, ", ",result)
  
  x := -42;
  switch {
    case x < 0:
      result = "less than zero"
      //fallthrough //ignore the case
    case x == 0:
      result = "equal to zero"
  }
  
  fmt.Println(result,x)
  
}