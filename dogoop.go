package main

import (
  "fmt"
)

type Dog struct {
  Breed string
  Weight int
  Sound string
}

func (d Dog) Speak(){ //method
  fmt.Println(d.Sound)
}

func (d Dog) SpeakThreeTimes(){ //if added *Dog passed the pointer which will affect the object
  d.Sound = fmt.Sprintf("%v ! %v ! %v ! ", d.Sound, d.Sound, d.Sound)
  fmt.Println(d.Sound)
}

func main(){
  
  poodle := Dog{"Poodle", 34, "Woof"}
  fmt.Println(poodle) // debug
  poodle.Speak()

  poodle.Sound = "Arf"
  poodle.Speak()
  
  poodle.SpeakThreeTimes()
  poodle.SpeakThreeTimes()
  
  
}