package main

import ("fmt")

func main(){
 primes := []int{2, 3, 5, 7, 11, 13,12}
 fmt.Println(isLastNumberEven( primes ) )
 fmt.Println(countAllEvenNumbers( primes ) )
 fmt.Println(findDuplicates( primes ) )
  
  fmt.Println("*** F1 ***")
  f := fibonacci()
  for i:=0; i<10; i++{
    fmt.Println(f())
  }
  
  fmt.Println("*** F2 ***")
  fibonacci2(10)  
  
  fmt.Println("*** F3 ***")
  c := fibonacci3()
  for n:=0; n<10; n++{
    fmt.Println( <- c)
  }
  
    c1 := fibonacci3()
    c2 := fibonacci3()

    // read first 10 numbers from 1st channel
    for n := 0; n < 10 ; n++ { fmt.Print(" ", <- c1) }
    fmt.Println()

    // read first 10 numbers from 2nd channel. The same.
    for n := 0; n < 10 ; n++ { fmt.Print(" ", <- c2) }
    fmt.Println()

    // read next   5 numbers from 1st channel.
    for n := 0; n <  5 ; n++ { fmt.Print(" ", <- c1) }
    fmt.Println()
    
    
    a := []int{7, 2, 8, -9, 4, 0, 1, -1}
    cc := make(chan int)
    go sum (a[:len(a)/2], cc)
    go sum (a[len(a)/2:], cc)
    x, y := <-cc, <-cc //recieve from c
    
    fmt.Println("x, y sum=",x, y, x+y)
  
}

func fibonacci() func() int{
  x := 0
  y := 1
  return func() int{
    x,y = y,x+y
    return x
  }
}

func fibonacci2( max int ) {
  x:=0
  y:=1
  n:=0
  for i:=0; i<max; i++{
    //x,y = y,x+y
      n = x+y
      x = y
      y = n
    fmt.Println(x)
  }
}

func fibonacci3() chan int{
  c := make(chan int)
  go func(){
    for i,j := 0,1; ; i,j =i+j, i{
      c <- i
    }
  }()
  return c
}

func sum( a []int, c chan int ){
  sum := 0
  for _, v := range a {
    sum += v
  }
  c <- sum //send sum to c
}

func isLastNumberEven( unsortedArray []int ) bool {
  length := len(unsortedArray);
  if unsortedArray[length-1] %2 == 0{
    return true
  }
  return false
}

func countAllEvenNumbers( unsortedArray []int ) int{
  evenNumberCount := 0
  for i:=0; i< len(unsortedArray); i++{
      evenNumberCount++
  }
  return evenNumberCount
}

//O(n2) order n square => quadratic time
func findDuplicates( unsortedArray []int ) bool{
  for i := range unsortedArray{
    for j := range unsortedArray{
      if(i == j){
        continue
      }
      if( unsortedArray[i] == unsortedArray[j] ){
         fmt.Println(unsortedArray[i],unsortedArray[j])
         return true 
      }
    }
  }
  return false
}