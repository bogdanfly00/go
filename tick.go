package main

import ("fmt"
        "time"
        "bufio"
        "os"
)

func main(){
  tick := time.Tick(100 * time.Millisecond)
  boom := time.After(1000 * time.Millisecond)
  for {
    select{
      case <-tick:
      fmt.Println("tick.")
      case <-boom:
        fmt.Println("BOOM!")
        reader := bufio.NewReader(os.Stdin)
        reader.ReadString('\n')
        return
      default:
        fmt.Println("    .")
        time.Sleep(50 * time.Millisecond)
    }
  }
  
}