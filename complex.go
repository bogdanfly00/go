package main

import ("fmt"
        "math/cmplx"
)

var (
  goIsFun bool  = true
  maxInt uint64 = 1<<64-1
  complex complex128 = cmplx.Sqrt(-5424523432.34422334 + 121290932.344234i) * cmplx.Asin(23423.2234543/32.2345345)
)

func main(){
  const f = "%T(%v)\n"
  const f2 = "(%v)\n"
  fmt.Printf(f, goIsFun, goIsFun)
  fmt.Printf(f, maxInt, maxInt)
  fmt.Printf(f, complex, complex)
  fmt.Printf(f2, complex)
}
