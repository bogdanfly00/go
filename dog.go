package main

import (
  "fmt"
)

type Dog struct {
  Breed string
  Weight int
}

func main(){
  
  poodle := Dog{"Poodle", 34}
  fmt.Printf("%+v\n", poodle) //debug with %+v
  fmt.Printf("Breed :%v\n Weight: %v", poodle.Breed, poodle.Weight)
  
}