package main

import (
  "fmt"
)

func main(){
  var colors [3]string
  colors[0] = "Red"
  colors[1] = "Blue"
  colors[2] = "Green"
  fmt.Println(colors)
  fmt.Println(colors[2])
  
  var numbers = [5]int{1,2,3,4,5}
  fmt.Println(numbers)
  
  fmt.Println("leng", len(colors))
  fmt.Println("leng", len(numbers))
  
  //slice are resisable
  
  var fruits = []string{"Apple", "Orange"}
  fmt.Println( fruits )
  
  fruits = append(fruits, "Bannana")
  fmt.Println( fruits )
  
}