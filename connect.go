package main

import "fmt"

var isConnected bool = false

func main(){
  fmt.Printf("Connection open: %v", isConnected)
  doSomething()
  fmt.Printf("Connection open: %v", isConnected)
}

func doSomething(){
  connect()
  fmt.Println("deffering disconnect")
  defer disconnect()
  fmt.Printf("Connection open: %v", isConnected)
  fmt.Println("Doing Something!")
}

func connect(){
  isConnected = true
  fmt.Println("Connected to db")
}

func disconnect(){
  isConnected = false
  fmt.Println("Disconnected")
}