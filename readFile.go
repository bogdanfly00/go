package main

import (
  "fmt"
  //"io"
  "io/ioutil"
  //"os"
)

func main(){
  fileName := "./fromString.txt"
  
  content, err := ioutil.ReadFile(fileName)
  checkError(err)
  
  fmt.Println("Read form file: ", content )
 
  result := string(content)
  fmt.Println("String :", result)
  
}

func checkError(err error){
  if err != nil{
    panic(err)
  }
}