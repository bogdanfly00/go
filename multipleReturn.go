package main

import "fmt"

func main(){
  n1, l1 := FullName("John", "Alice")
  fmt.Printf("Fullname %v number of chars %v",n1, l1 )
  n2, l2 := FullNameNakedReturn("Doe", "McNeil")
  fmt.Printf("Fullname %v number of chars %v",n2, l2 )
}

func FullName(f,l string) (string,int){
  full := f + " " + l
  length := len(full)
  return full,length
  
}

func FullNameNakedReturn(f,l string) (full string, lenght int){
  full = f + " " + l // removed := because is been declared above
  lenght = len(full)
  return 
}