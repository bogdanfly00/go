package main

import (
  "fmt"
  "io"
  "io/ioutil"
  "os"
)

func main(){
  content := "hello from go!"
  file, err := os.Create("./fromString.txt")
  checkError(err)
  defer file.Close()
  
  ln, err := io.WriteString(file,content)
  checkError(err)
  
  fmt.Printf("all done of %v ", ln)
  
  bytes := []byte(content) //convert content to byte array!!
  ioutil.WriteFile("./fromBytes.txt", bytes, 0644)
  
}

func checkError(err error){
  if err != nil{
    panic(err)
  }
}